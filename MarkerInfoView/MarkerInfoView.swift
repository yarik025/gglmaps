//
//  MarkerInfoView.swift
//  GglMaps
//
//  Created by Yaroslav Georgievich on 22.06.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit

class MarkerInfoView: UIView {

    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var contentInfoView: UIView!
}
