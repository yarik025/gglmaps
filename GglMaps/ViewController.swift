//
//  ViewController.swift
//  GglMaps
//
//  Created by Yaroslav Georgievich on 20.06.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit
import GoogleMaps
import GooglePlaces

class ViewController: UIViewController {
    
    @IBOutlet weak var adressLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    var locationManager = CLLocationManager()
    var zoomLevel: Float = 15.0

    override func viewDidLoad() {
        super.viewDidLoad()
        setCurrentLocation()
    }
}

extension ViewController: CLLocationManagerDelegate, GMSMapViewDelegate{
    func setCurrentLocation() {
        
        //MFMailComposeViewControllerDelegate нужен для Email
        //MFMessageComposeViewControllerDelegate нужен для SMS
        
        //Иннициализация location менеджера
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        
        mapView.delegate = self
        
        //рисует маркер по твоим координатам
        //кнопка для центровки камеры по твоим коорд
        mapView.isMyLocationEnabled = true
        mapView.settings.myLocationButton = true
    }
    
    func mapView(_ mapView: GMSMapView, didTap marker: GMSMarker) -> Bool {
        //Выполняеться после нажатие на маркер
        
        print("`Did tap on marker")
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let userInfoVC = storyBoard.instantiateViewController(withIdentifier: "userInfo") as! UserInfoViewController

        if let data = marker.userData as? User {
            userInfoVC.user = data
            
            self.present(userInfoVC, animated: true, completion:nil)
            return true
        } else {
            print("User is empty")
            return true
        }
    }
    
    func setMarker() {
        let users = [
            User(name: "Алексей", surname: "Марфутин", age: "25", phone: "+380666817073", long: 30.557384, lat: 50.405211, image: UIImage(named: "Alexey")!, address: "ул. Миколи Соловцова", email: "yarik.p@mail.ru"),
            User(name: "Лилия", surname: "Несина", age: "22", phone: "+380997792763", long: 30.450482, lat: 50.449124, image: UIImage(named: "Lilia")!, address: "ул. Металлистов 8", email: "yarik.p@mail.ru"),
            User(name: "Вячеслав", surname: "Савицкий", age: "20", phone: "+380951853085", long: 30.465319, lat: 50.422534, image: UIImage(named: "Slava")!, address: "ул. Лобановского 6а", email: "yarik.p@mail.ru"),
            User(name: "Толик", surname: "Гордиенко", age: "28", phone: "+380939817010", long: 30.520566, lat: 50.492685, image: UIImage(named: "Tolik")!, address: "ул. Приозерная 2а", email: "yarik.p@mail.ru"),
            User(name: "Ярик", surname: "Павлюк", age: "26", phone: "+380636239465", long: 30.425612, lat: 50.445742, image: UIImage(named: "Yarik")!, address: "ул. Вацлава Гавела 9а", email: "yarik.p@mail.ru")
        ]
        
        for user in users {
            let marker = GMSMarker()
            marker.position = CLLocationCoordinate2D(latitude: user.lat, longitude: user.long)
            marker.map = mapView
            marker.userData = user
            
            let markerImage = UIImageView(frame: CGRect(x: 0, y: 0, width: 50, height: 50))
            markerImage.image = UIImage(named: "pinsicon")
            marker.iconView = markerImage
        }
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        //Выполняеться когда менеджер получает новые данные
        
        guard locations.first != nil else { return }
        
        let camera = GMSCameraPosition.camera(withLatitude: 50.449816, longitude: 30.420533, zoom: zoomLevel)
        setMarker()
        
        if mapView.isHidden {
            self.mapView.isHidden = false
            self.mapView.camera = camera
        } else {
            self.mapView.animate(to: camera)
        }
        
        self.locationManager.stopUpdatingLocation()
        
    }
    
    // Handle authorization for the location manager.
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .restricted:
            print("Location access was restricted.")
        case .denied:
            print("User denied access to location.")
            // Display the map using the default location.
            mapView.isHidden = false
        case .notDetermined:
            print("Location status not determined.")
        case .authorizedAlways: fallthrough
        case .authorizedWhenInUse:
            print("Location status is OK.")
        }
    }
    
    // Handle location manager errors.
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        locationManager.stopUpdatingLocation()
        print("Error: \(error)")
    }
}
