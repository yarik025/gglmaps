//
//  User.swift
//  GglMaps
//
//  Created by Yaroslav Georgievich on 21.06.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import Foundation
import GoogleMaps

struct User {
    let name: String
    let surname: String
    let age: String
    let phone: String
    let long: CLLocationDegrees
    let lat: CLLocationDegrees
    let image: UIImage
    let address: String
    let email: String
}
