//
//  UserInfoViewController.swift
//  GglMaps
//
//  Created by Yaroslav Georgievich on 25.06.2019.
//  Copyright © 2019 Pavliuk. All rights reserved.
//

import UIKit
import MessageUI

class UserInfoViewController: UIViewController {
    
    @IBOutlet var contentView: UIView!
    @IBOutlet weak var userInfoView: UIView!
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var surnameLabel: UILabel!
    @IBOutlet weak var ageLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    
    @IBOutlet weak var phoneButton: UIButton!
    @IBOutlet weak var emailButton: UIButton!
    
    var user: User?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        contentView.backgroundColor = UIColor.gray.withAlphaComponent(0.5)
        userInfoView.layer.cornerRadius = 24
        
        nameLabel.text = user?.name ?? "-"
        surnameLabel.text = user?.surname ?? "-"
        avatarImage.image = user?.image ?? UIImage()
        ageLabel.text = user?.age ?? "-"
        addressLabel.text = user?.address ?? "-"
        
        phoneButton.setTitle(user?.phone, for: .normal)
        phoneButton.addTarget(self, action:#selector(handleRegisterPhone(sender:)), for: .touchUpInside)
        emailButton.setTitle(user?.email, for: .normal)
        emailButton.addTarget(self, action:#selector(handleRegisterEmail(sender:)), for: .touchUpInside)
        
    }
    
    @IBAction func closeUserInfoButton(_ sender: Any) {
        self.dismiss(animated: false, completion: nil)
        print("did close userInfo")
    }
}

extension UserInfoViewController: MFMailComposeViewControllerDelegate,  MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result:
        //Выполняеться когда отправил или отменил отправку
        
        MessageComposeResult) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        controller.dismiss(animated: true, completion: nil)
    }
    
    func sendMessage(phone: String, name: String) {
        let messageCompose = MFMessageComposeViewController()
        messageCompose.messageComposeDelegate = self
        messageCompose.recipients = [phone]
        messageCompose.body = "Hello, \(name)"
        messageCompose.subject = "Check"
        present(messageCompose, animated: true, completion: nil)
    }
    
    func sendMail(email: String, name: String) {
        let composeVC = MFMailComposeViewController()
        composeVC.mailComposeDelegate = self
        composeVC.setToRecipients([email])
        composeVC.setSubject("Hello, \(name)")
        composeVC.setMessageBody("SOMETHING MUST BE HERE", isHTML: false)
        present(composeVC, animated: true, completion: nil)
    }
    
    @objc func handleRegisterPhone(sender: UIButton){
        sendMessage(phone: user?.phone ?? "+380505584120", name: user?.name ?? "app")
    }
    
    @objc func handleRegisterEmail(sender: UIButton){
        sendMail(email: user?.email ?? "yarik.p@mail.ru", name: user?.name ?? "app")
    }
}
